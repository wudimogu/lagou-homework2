package com.zou.edu.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class LoginController {

    @GetMapping(value = "/login")
    public String login(HttpServletRequest request, ModelMap modelMap) {
        modelMap.addAttribute("port", request.getLocalPort());
        return "login";
    }

    @PostMapping(value = "/toLogin")
    public String toLogin(String username, String password, HttpServletRequest request, HttpSession session, ModelMap modelMap) {

        if (!"admin".equals(username) || !"admin".equals(password)) {
            return "login";
        }
        session.setAttribute("token", "token");
        modelMap.addAttribute("port", request.getLocalPort());
        return "home";
    }

    @GetMapping(value = "/home")
    public String home(HttpServletRequest request, ModelMap modelMap) {
        modelMap.addAttribute("port", request.getLocalPort());
        return "home";
    }
}
