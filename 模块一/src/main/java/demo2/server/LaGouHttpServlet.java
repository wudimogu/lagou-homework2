package demo2.server;

import com.zou.server.Request;
import com.zou.server.Response;
import com.zou.service.HttpServlet;

import java.io.IOException;

/**
 * 自定义servlet
 */
public class LaGouHttpServlet extends HttpServlet {

    @Override
    public void doGet(Request request, Response response) throws IOException {
        doPost(request, response);
    }

    @Override
    public void doPost(Request request, Response response) throws IOException {
        String result = "demo2.LaGouHttpServlet Response...";
        response.writeSuccess(result.getBytes().length);
        response.write(result.getBytes());
    }
}
