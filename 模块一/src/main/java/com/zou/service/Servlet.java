package com.zou.service;

import com.zou.server.Request;
import com.zou.server.Response;

import java.io.IOException;

public interface Servlet {
    public void init();

    public void service(Request request, Response response) throws IOException;

    public void destory();
}
