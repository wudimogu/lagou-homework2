package com.zou.service;

import com.zou.server.CommonUtil;
import com.zou.server.Request;
import com.zou.server.Response;

import java.io.IOException;

public abstract class HttpServlet implements Servlet {

    public abstract void doGet(Request request, Response response) throws IOException;

    public abstract void doPost(Request request, Response response) throws IOException;

    @Override
    public void service(Request request, Response response) throws IOException {
        if (CommonUtil.GET.equals(request.getMethod())) {
            doGet(request, response);
        } else {
            doPost(request, response);
        }
    }

    @Override
    public void init() {

    }

    @Override
    public void destory() {

    }
}
