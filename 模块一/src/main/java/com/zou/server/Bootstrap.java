package com.zou.server;

import com.zou.mapper.Mapper;
import com.zou.service.HttpServlet;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 启动类
 */
public class Bootstrap {

    // 默认web.xml文件名称
    public static final String SERVER_XML = "server.xml";
    // 默认web.xml文件名称
    public static final String WEB_XML = "web.xml";

    // port
    private Integer port;

    // 线程池
    private ThreadPoolExecutor threadPoolExecutor;
    // Host->Context->Wrapper->Servlet
    private Mapper.Host mapperHost;

    /**
     * 启动方法
     */
    public static void main(String[] args) {
        Bootstrap bootstrap = new Bootstrap();
        try {
            // 初始化方法
            bootstrap.load();
            // 启动方法
            bootstrap.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 初始化方法
     * 1.初始化对应的mapper
     * 2.初始化线程池
     */
    private void load() {
        // 初始化对应的mapper
        initMapper();
        // 初始化线程池
        initThreadPool();
    }

    /**
     * 初始化对应的mapper
     */
    private void initMapper() {
        // 初始化server
        initServer();
    }

    /**
     * 加载server
     */
    private void initServer() {
        InputStream serverXmlStream = Bootstrap.class.getClassLoader().getResourceAsStream(SERVER_XML);
        SAXReader saxReader = new SAXReader();
        try {
            Document document = saxReader.read(serverXmlStream);
            Element rootElement = document.getRootElement();

            Node service = rootElement.selectSingleNode("Service");
            // 设置connector
            Node connector = service.selectSingleNode("Connector");
            this.port = Integer.valueOf(((Element) connector).attributeValue("port"));
            // 设置host
            Node host = service.selectSingleNode("Engine/Host");
            String hostName = ((Element) host).attributeValue("name");
            mapperHost = new Mapper.Host(hostName);

            String appBase = ((Element) host).attributeValue("appBase");
            // 设置context
            File file = new File(appBase);
            if (!file.exists() || !file.isDirectory()) {
                throw new RuntimeException("appBase is not exist or is not a directory");
            }
            List<Mapper.Context> contextList = new ArrayList<>();
            for (File contextFile : file.listFiles()) {
                // 设置context
                Mapper.Context context = new Mapper.Context(contextFile.getName());
                // 循环设置wrapper
                for (File wrapperFile : contextFile.listFiles()) {
                    // 解析web.xml
                    if (wrapperFile.getName().equals(WEB_XML)) {
                        context.setWrapperList(initServlet(wrapperFile));
                        break;
                    }
                }
                contextList.add(context);
            }
            mapperHost.setContextList(contextList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 加载servlet
     */
    private List<Mapper.Wrapper> initServlet(File xmlFile) throws FileNotFoundException {
        List<Mapper.Wrapper> wrapperList = new ArrayList<>();

        InputStream webXmlStream = new FileInputStream(xmlFile);
        SAXReader saxReader = new SAXReader();
        try {
            Document document = saxReader.read(webXmlStream);
            Element rootElement = document.getRootElement();

            List<Element> servletList = rootElement.selectNodes("//servlet");
            for (Element servletElement : servletList) {
                // 获取servlet标签属性
                Node servletNameNode = servletElement.selectSingleNode("servlet-name");
                Node servletClassNode = servletElement.selectSingleNode("servlet-class");
                // 实例化servlet
                HttpServlet httpServlet = (HttpServlet) Class.forName(servletClassNode.getText()).getDeclaredConstructor().newInstance();
                // 获取servlet-mapping属性
                Node servletMappingNode = rootElement.selectSingleNode("/web-app/servlet-mapping[servlet-name='" + servletNameNode.getText() + "']");

                // 设置wrapper
                Mapper.Wrapper wrapper = new Mapper.Wrapper(servletMappingNode.selectSingleNode("url-pattern").getText());
                wrapper.setHttpServlet(httpServlet);
                wrapperList.add(wrapper);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return wrapperList;
    }

    /**
     * 初始化线程池
     */
    private void initThreadPool() {
        this.threadPoolExecutor = new ThreadPoolExecutor(
                2, 5,
                100L, TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(5),
                Executors.defaultThreadFactory(),
                new ThreadPoolExecutor.AbortPolicy());
    }

    /**
     * 启动方法
     * <p>
     * 1.0版本：单纯返回html页面
     * 2.0版本：封装request和response对象，返回html页面
     * 3.0版本：使用线程池并可以请求动态servlet
     * 4.0版本：模拟出webapps部署效果 磁盘上放置一个webapps目录，webapps中可以有多个项目，例如demo1、demo2、demo3... 每个项目中含有servlet，可以根据请求url定位对应servlet进一步处理。
     */
    @SuppressWarnings("InfiniteLoopStatement")
    private void start() throws IOException {
        ServerSocket serverSocket = new ServerSocket(port);
        while (true) {
            Socket accept = serverSocket.accept();
            threadPoolExecutor.execute(new RequestProcessor(accept, mapperHost));
        }
    }

}
