package com.zou.server;

import java.io.IOException;
import java.io.InputStream;

public class Request {

    /**
     * GET OR POST
     */
    private String method;

    /**
     * 访问地址
     */
    private String url;

    /**
     * 参数
     */
    private String params;

    /**
     * 请求内容
     */
    private String content;

    /**
     * 输入流
     */
    private InputStream inputStream;

    public Request(InputStream inputStream) throws IOException {
        this.inputStream = inputStream;
        String content = CommonUtil.read(inputStream);
        // GET /demo/query1?username=zhangsan&name=lisi HTTP/1.1
        String firstLine = content.split("\\n")[0];
        String[] firstLineSplit = firstLine.split(" ");
        this.method = firstLineSplit[0];
        if (firstLineSplit[1].contains("?")) {
            this.url = firstLineSplit[1].split("\\?")[0];
            this.params = firstLineSplit[1].split("\\?")[1];
        }else {
            this.url = firstLineSplit[1];
        }
    }

    public String getMethod() {
        return method;
    }

    public Request setMethod(String method) {
        this.method = method;
        return this;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public Request setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
        return this;
    }

    public String getUrl() {
        return url;
    }

    public Request setUrl(String url) {
        this.url = url;
        return this;
    }

    public String getContent() {
        return content;
    }

    public Request setContent(String content) {
        this.content = content;
        return this;
    }

    public String getParams() {
        return params;
    }

    public Request setParams(String params) {
        this.params = params;
        return this;
    }
}
