package com.zou.server;

import com.zou.mapper.Mapper;

import java.io.IOException;
import java.net.Socket;

/**
 * request 请求处理器
 */
public class RequestProcessor implements Runnable {
    // socket
    private final Socket socket;
    // request
    private final Request request;
    // response
    private final Response response;
    // Host->Context->Wrapper->Servlet
    private Mapper.Host mapperHost;

    public RequestProcessor(Socket socket, Mapper.Host mapperHost) throws IOException {
        this.socket = socket;
        this.request = new Request(socket.getInputStream());
        this.response = new Response(socket.getOutputStream());
        this.mapperHost = mapperHost;
    }

    @Override
    public void run() {
        System.out.println("-------------------线程：" + Thread.currentThread().getName() + "开始处理请求-------------------");
        try {
            // /index.html  /demo1/laGou /demo2/laGou
            String[] urlSplit = request.getUrl().split("/", 3);
            if (urlSplit.length <= 2) {
                // 请求MiniCat文件
                if (urlSplit.length == 2) {
                    response.writeHtml(urlSplit[1]);
                } else {
                    response.writeDefaultHtml();
                }
            } else {
                boolean isMatch = false;
                // 请求部署的项目
                for (Mapper.Context context : mapperHost.getContextList()) {
                    if (context.name.equals(urlSplit[1])) {
                        // 项目
                        for (Mapper.Wrapper wrapper : context.getWrapperList()) {
                            if (wrapper.name.equals("/" + urlSplit[2])) {
                                isMatch = true;
                                wrapper.getHttpServlet().service(request, response);
                                break;
                            }
                        }
                    }
                    if (isMatch) break;
                }
                // 判断servlet还是html
                if (!isMatch) response.writeHtml(request.getUrl());
            }
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
