package com.zou.server;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

public class Response {
    // 默认html路径
    public static final String DEFAULT_HTML = "index.html";

    private OutputStream outputStream;

    public Response(OutputStream outputStream) {
        this.outputStream = outputStream;
    }

    /**
     * 正常返回
     */
    public void writeSuccess(long contentLength) throws IOException {
        String success = "HTTP/1.1 200 OK \n" +
                "Content-Type: text/html;charset=utf-8 \n" +
                "Content-Length: " + contentLength + " \n" +
                "\r\n";
        outputStream.write(success.getBytes());
    }

    /**
     * 正常默认页面返回页面
     */
    public void writeDefaultHtml() throws IOException {
        writeHtml(DEFAULT_HTML);
    }

    /**
     * 正常返回页面
     */
    public void writeHtml(String path) throws IOException {
        String absolutePath = CommonUtil.getAbsolutePath();

        File file = new File(absolutePath + path);
        if (!file.exists() || file.isDirectory()) {
            write404();
            return;
        }
        System.out.println("-------------------返回静态资源-------------------");
        byte[] bytes = CommonUtil.read(new FileInputStream(file)).getBytes();
        writeSuccess(bytes.length);
        write(bytes);
    }

    /**
     * 返回404页面
     */
    public void write404() throws IOException {
        String str404 = "<h1>404 not found</h1>";
        write(("HTTP/1.1 404 NOT Found \n" +
                "Content-Type: text/html \n" +
                "Content-Length: " + str404.getBytes().length + " \n" +
                "\r\n" + str404).getBytes());
    }

    /**
     * 返回输出
     */
    public void write(byte[] result) throws IOException {
        outputStream.write(result);
    }

}
