package com.zou.server;

import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;

/**
 * 通用公共类
 */
public class CommonUtil {

    public static final String GET = "GET";

    public static final String POST = "POST";

    /**
     * 通过输入流，读取信息
     */
    public static String read(InputStream inputStream) throws IOException {
        int available = 0;
        while (available == 0) {
            available = inputStream.available();
        }

        byte[] bytes = new byte[available];
        inputStream.read(bytes);

        return new String(bytes);
    }

    /**
     * 获取绝对路径
     */
    public static String getAbsolutePath() {
        String absolutePath = CommonUtil.class.getClassLoader().getResource("").getPath();
        return URLDecoder.decode(absolutePath, StandardCharsets.UTF_8);
    }
}
