package com.zou.mapper;

import com.zou.service.HttpServlet;

import java.util.List;

/**
 * http://localhost:8080/demo1/lagou
 * Mapper组件体系
 * Mapper类—>Host->Context->Wrapper->Servlet
 */
public class Mapper {

    /**
     * http://localhost:8080/demo1/lagou
     * 表示：/8080
     */
    public static class Host extends MapElement{

        private List<Context> contextList;

        public Host(String name) {
            super(name);
        }

        public List<Context> getContextList() {
            return contextList;
        }

        public Host setContextList(List<Context> contextList) {
            this.contextList = contextList;
            return this;
        }
    }

    /**
     * http://localhost:8080/demo1/lagou
     * 表示：/demo1
     */
    public static class Context extends MapElement{

        private List<Wrapper> wrapperList;

        public Context(String name) {
            super(name);
        }

        public List<Wrapper> getWrapperList() {
            return wrapperList;
        }

        public Context setWrapperList(List<Wrapper> wrapperList) {
            this.wrapperList = wrapperList;
            return this;
        }
    }

    /**
     * http://localhost:8080/demo1/lagou
     * 表示：/lagou
     */
    public static class Wrapper extends MapElement{

        private HttpServlet httpServlet;

        public Wrapper(String name) {
            super(name);
        }

        public HttpServlet getHttpServlet() {
            return httpServlet;
        }

        public Wrapper setHttpServlet(HttpServlet httpServlet) {
            this.httpServlet = httpServlet;
            return this;
        }
    }

    /**
     * 公共基类
     */
    protected abstract static class MapElement {

        public final String name;

        public MapElement(String name) {
            this.name = name;
        }
    }
}
